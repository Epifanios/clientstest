<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.head')
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            @include('layouts.header')

            @include('layouts.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Add Employee</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{ url('/employeeslist') }}">Employees List</a></li>
                                    <li class="breadcrumb-item active">Add Employee</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Add Employee</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(Session::has('flash_message_success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('flash_message_success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <form class="form-horizontal" action="{{ url('add-employee') }}" name="AddEmployeeForm" id="AddEmployeeForm">
                        <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group pt-4">
                                        <label for="employee_name">Employee Name</label>
                                        <input type="text" class="form-control" id="employee_name" name="employee_name" placeholder="Name">
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="employee_salary">Employee Salary</label>
                                        <input type="text" class="form-control" id="employee_salary" name="employee_salary" placeholder="Salary">
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="employee_age">Employee Age</label>
                                        <input type="text" class="form-control" id="employee_age" name="employee_age" placeholder="Age">
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="profile_image">Profile Image</label>
                                        <br>
                                        <input type="file" class="profile_image" id="profile_image" name="profile_image">
                                    </div>

                                    <div class="box-footer pt-4 mb-5">
                                        <button type="submit" class="btn btn-info" id="butsave">Submit</button>
                                    </div>
                                </div>
                                <!-- /.col-->
                            </div>
                            <!-- /.row -->
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.content-wrapper -->
            @include('layouts.footer')

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        @include('layouts.scripts')
        <script>
            $(document).ready(function() {

                $('#butsave').on('click', function(e) {
                  var employee_name = $('#employee_name').val();
                  var employee_salary = $('#employee_salary').val();
                  var employee_age = $('#employee_age').val();
                  var profile_image = $('#profile_image').val();

                  if(employee_name!="" && employee_salary!="" && employee_age!="" && profile_image!=""){
                    /*  $("#butsave").attr("disabled", "disabled"); */
                    e.preventDefault();

                      $.ajax({
                          url: "http://dummy.restapiexample.com/api/v1/create",
                          type: "POST",
                          data: {
                              _token: $("#csrf").val(),
                              type: 1,
                              employee_name: employee_name,
                              employee_salary: employee_salary,
                              employee_age: employee_age,
                              profile_image: profile_image
                          },
                          cache: false,
                          success: function(dataResult){
                              console.log(dataResult);
                              alert(dataResult.message)
                            //   var dataResult = JSON.parse(dataResult);
                            //   if(dataResult.statusCode==200){
                            //     alert("Employee added suscessfully!");
                            //   }
                            //   else if(dataResult.statusCode==201){
                            //      alert("Error occured !");
                            //   }

                          }
                      });
                  }
                  else{
                      alert('Please fill all the field !');
                  }
              });
            });
            </script>
    </body>
</html>
