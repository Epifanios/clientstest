
<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('layouts.header')

        @include('layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Employees List</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-12">

                    @if(Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('success_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div class="card">
                        <div class="form-row card-header w-100">
                            {{-- <div class="col-md-6 col-sm-12 my-1 mr-auto">
                                <form class="form-inline" action="{{ url('search-results-clients') }}" method="GET">
                                    <input type="text" name="search_clients" class="form-control" placeholder="Αναζήτηση"> <br>
                                    <button type="submit" class="btn btn-info"><i class="fas fa-search fa-fw"></i></button>
                                </form>
                            </div>
                         --}}
                            {{-- <div class="add-client-button">
                                <a href="{{ url('add-employee') }}" class="btn btn-info">Add Employee</a>
                            </div> --}}
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-horizontal" action="{{ url('employeeslist') }}" name="AddEmployeeForm" id="AddEmployeeForm" method="post" role="form" enctype="multipart/form-data">
                                <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group pt-4">
                                                <label for="employee_name">Employee Name</label>
                                                <input type="text" class="form-control" id="employee_name" name="employee_name" placeholder="Name">
                                            </div>

                                            <div class="form-group pt-4">
                                                <label for="employee_salary">Employee Salary</label>
                                                <input type="text" class="form-control" id="employee_salary" name="employee_salary" placeholder="Salary">
                                            </div>

                                            <div class="form-group pt-4">
                                                <label for="employee_age">Employee Age</label>
                                                <input type="text" class="form-control" id="employee_age" name="employee_age" placeholder="Age">
                                            </div>

                                            <div class="form-group pt-4">
                                                <label for="profile_image">Profile Image</label>
                                                <br>
                                                <input type="file" class="profile_image" id="profile_image" name="profile_image">
                                            </div>

                                            <div class="box-footer pt-4 mb-5">
                                                <button type="submit" class="btn btn-info" id="butsave">Submit</button>
                                            </div>
                                        </div>
                                        <!-- /.col-->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </form>

                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Employee Name</th>
                                        <th>Employee Salary</th>
                                        <th>Employee Age</th>
                                        <th>Profile Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyData">


                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->
  @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

    @include('layouts.scripts')

    <script type="text/javascript">

        function getAllEmployees() {
            $.ajax({
                url: "http://dummy.restapiexample.com/api/v1/employees",
                type: "GET",
                data:{
                    _token:'{{ csrf_token() }}'
                },
                cache: false,
                dataType: 'json',
                success: function(dataResult){
                    //console.log(dataResult);
                    var resultData = dataResult.data;
                    var bodyData = '';
                    //var i=1;
                    //i++
                    $.each(resultData,function(index,row){
                        bodyData+='<tr>'
                        bodyData+='<td>'+row.id+'</td><td>'+row.employee_name+'</td><td>'+row.employee_salary+'</td><td>'+row.employee_age+'</td><td>'+row.profile_image+'</td><td><button class="action-buttons mr-2" onclick="updateEmployee('+row.id+')"><i class="fas fa-edit"></i></button><button class="action-buttons" onclick="deleteEmployee('+row.id+')"><i class="fas fa-trash-alt"></i></button></td>';
                        bodyData+='</tr>';

                    })
                    $("#bodyData").append(bodyData);
                }
            });
        }



        $(document).ready(function(){
            getAllEmployees()
        });



        $('#butsave').on('click', function(e) {
            var employee_name = $('#employee_name').val();
            var employee_salary = $('#employee_salary').val();
            var employee_age = $('#employee_age').val();
            var profile_image = $('#profile_image').val();

            if(employee_name!="" && employee_salary!="" && employee_age!="" && profile_image!=""){
            /*  $("#butsave").attr("disabled", "disabled"); */
                e.preventDefault();

                $.ajax({
                    url: "http://dummy.restapiexample.com/api/v1/create",
                    type: "POST",
                    data: {
                        _token: $("#csrf").val(),
                        employee_name: employee_name,
                        employee_salary: employee_salary,
                        employee_age: employee_age,
                        profile_image: profile_image
                    },
                    cache: false,
                    success: function(dataResult){
                        console.log(dataResult);
                        alert(dataResult.message)
                    }
                });
            }
            else{
                alert('Please fill all the field !');
            }
        });



        function deleteEmployee(id) {
            $.ajax({
                url: "http://dummy.restapiexample.com/public/api/v1/delete/" + id,
                type: "DELETE",
                dataType: "json",
                success: function(dataResult){
                    console.log(dataResult);
                    alert(dataResult.message);
                },
                error: function(error){
                    alert("Employee is not deleted");
                }
            });
        }



        function updateEmployee(id) {
            $.ajax({
                url: "http://dummy.restapiexample.com/public/api/v1/update/" + id,
                type: "PUT",
                success: function(dataResult){
                    console.log(dataResult);
                    $('#employee_name').val(dataResult.employee_name);
                    $('#employee_salary').val(dataResult.employee_salary);
                    $('#employee_age').val(dataResult.employee_age);
                    $('#profile_image').val(dataResult.profile_image);
                    getAllEmployees();
                },
                error: function(error){
                    alert("Employee is not updated");
                }
            });
        }



        function reset() {
            $('#employee_name').val('');
            $('#employee_salary').val('');
            $('#employee_age').val('');
            $('#profile_image').val('');
        }
    </script>
</body>
</html>
