<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.head')
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            @include('layouts.header')

            @include('layouts.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">{{ $title }}</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{ url('/clientslist') }}">Clients List</a></li>
                                    <li class="breadcrumb-item active">{{ $title }}</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">{{ $title }}</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(Session::has('flash_message_success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('flash_message_success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <form class="form-horizontal"
                    @if (empty($clientData['id']))
                        action="{{ url('add-edit-client') }}"
                    @else
                        action="{{ url('add-edit-client/'.$clientData['id']) }}"
                    @endif
                    name="AddEditClientForm" id="AddEditClientForm" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group pt-4">
                                        <label for="client_name">Name</label>
                                        <input type="text" class="form-control" id="client_name" name="client_name" placeholder="Name"
                                        @if (!empty($clientData['name']))
                                            value="{{ $clientData['name'] }}"
                                        @else
                                            value="{{ old('client_name') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="client_email">Email</label>
                                        <input type="text" class="form-control" id="client_email" name="client_email" placeholder="Email"
                                        @if (!empty($clientData['email']))
                                            value="{{ $clientData['email'] }}"
                                        @else
                                            value="{{ old('client_email') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="mobile_phone">Mobile Phone</label>
                                        <input type="text" class="form-control" id="mobile_phone" name="mobile_phone" placeholder="Mobile Phone"
                                        @if (!empty($clientData['mobile_phone']))
                                            value="{{ $clientData['mobile_phone'] }}"
                                        @else
                                            value="{{ old('mobile_phone') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="client_post_code">Post Code</label>
                                        <input type="text" class="form-control" id="client_post_code" name="client_post_code" placeholder="Post Code"
                                        @if (!empty($clientData['post_code']))
                                            value="{{ $clientData['post_code'] }}"
                                        @else
                                            value="{{ old('client_post_code') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label>Birthday</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="text" class="form-control" id="client_birthday" name="client_birthday" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask
                                            @if (!empty($clientData['birthday']))
                                                value="{{ $clientData['birthday'] }}"
                                            @else
                                                value="{{ old('client_birthday') }}"
                                            @endif
                                            >
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group pt-4">
                                        <label for="client_surname">Surname</label>
                                        <input type="text" class="form-control" id="client_surname" name="client_surname" placeholder="Surname"
                                        @if (!empty($clientData['surname']))
                                            value="{{ $clientData['surname'] }}"
                                        @else
                                            value="{{ old('client_surname') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="work_phone">Work Phone</label>
                                        <input type="text" class="form-control" id="work_phone" name="work_phone" placeholder="Work Phone"
                                        @if (!empty($clientData['work_phone']))
                                            value="{{ $clientData['work_phone'] }}"
                                        @else
                                            value="{{ old('work_phone') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="client_address">Address</label>
                                        <input type="text" class="form-control" id="client_address" name="client_address" placeholder="Address"
                                        @if (!empty($clientData['address']))
                                            value="{{ $clientData['address'] }}"
                                        @else
                                            value="{{ old('client_address') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label for="client_country">Country</label>
                                        <input type="text" class="form-control" id="client_country" name="client_country" placeholder="Country"
                                        @if (!empty($clientData['country']))
                                            value="{{ $clientData['country'] }}"
                                        @else
                                            value="{{ old('client_country') }}"
                                        @endif
                                        >
                                    </div>

                                    <div class="form-group pt-4">
                                        <label>Select Services</label>
                                        <select class="form-control select2" style="width: 100%;" name="service_id[]" id="service_id" multiple="multiple" data-placeholder="Select Services">
                                            <option value="">Select</option>
                                            @foreach ($services as $service)
                                                <option value="{{ $service['id'] }}"
                                                    {{in_array($service['id'], $selectedServices) ? 'selected':''}}

                                                    @if($errors->any())
                                                        {{in_array($service['id'], old("service_id") ?: []) ? "selected": ""}}
                                                    @endif
                                                >{{ $service['service_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col-->

                                <div class="col-sm-12">
                                    <div class="box-footer pt-4 mb-5">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.content-wrapper -->
            @include('layouts.footer')

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        @include('layouts.scripts')
        <script>
            $(function () {
                $('#client_birthday').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            });

            //Confirm Delete Record
            $(document).ready(function() {
                $(".confirmDelete").click(function() {
                    var name = $(this).attr("name");

                    if(confirm("Are you sure to delete this "+ name +"?")) {
                        return true;
                    }
                    return false;
                });
            });
        </script>
    </body>
</html>
