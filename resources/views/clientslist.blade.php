
<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('layouts.header')

        @include('layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Clients List</h1>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-12">

                    @if(Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('success_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div class="card">
                        <div class="form-row card-header w-100">
                            <div class="col-md-6 col-sm-12 my-1 mr-auto">
                                <form class="form-inline" action="{{ url('search-results-clients') }}" method="GET">
                                    <input type="text" name="search_clients" class="form-control" placeholder="Αναζήτηση"> <br>
                                    <button type="submit" class="btn btn-info"><i class="fas fa-search fa-fw"></i></button>
                                </form>
                            </div>
                        
                            <div class="add-client-button">
                                <a href="{{ url('add-edit-client') }}" class="btn btn-info">Add Client</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Surname</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($clients as $client)
                                        <tr>
                                            <div class="modal fade" id="myModal{{$client->id}}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-info">
                                                                <h5 class="modal-title" id="exampleModalLabel">More Information for {{$client->name}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    @if (!empty($client->mobile_phone))
                                                                        <b>Mobile Phone:</b> {{$client->mobile_phone}}
                                                                    @endif                                                                        
                                                                </p>

                                                                <p>
                                                                    @if (!empty($client->work_phone))
                                                                        <b>Work Phone:</b> {{$client->work_phone}}
                                                                    @endif                                                                        
                                                                </p>

                                                                <p>
                                                                    @if (!empty($client->post_code))
                                                                        <b>Post Code:</b> {{$client->post_code}}
                                                                    @endif                                                                        
                                                                </p>

                                                                <p>
                                                                    @if (!empty($client->address))
                                                                        <b>Address:</b> {{$client->address}}
                                                                    @endif                                                                        
                                                                </p>

                                                                <p>
                                                                    @if (!empty($client->birthday))
                                                                        <b>Birthday:</b> {{$client->birthday}}
                                                                    @endif                                                                        
                                                                </p>

                                                                <p>
                                                                    <b>Services: </b>
                                                                    @foreach ($client->clients_services as $service)
                                                                        {{ $loop->first ? '' : ', ' }}
                                                                        {{ $service->service_name }}
                                                                    @endforeach                                                                        
                                                                </p>
                                                            </div>                                     
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <td>{{ $client->id }}</td>
                                            <td>{{ $client->name }}</td>
                                            <td>{{ $client->surname }}</td>
                                            <td>{{ $client->email }}</td>
                                            <td>
                                                <a title="More Information" class="view-more" data-toggle="modal" data-target="#myModal{{$client->id}}" data-id="{{$client->id}}"><i class="fas fa-eye pr-2"></i></a>
                                                <a title="Edit Client" href="{{ url('add-edit-client/'.$client->id) }}"><i class="fas fa-edit pr-2"></i></a>
                                                <a title="Delete Client" class="confirmDelete" name="Client" href="{{ url('delete-client/'.$client->id) }}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->
  @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

    @include('layouts.scripts')

    
</body>
</html>
