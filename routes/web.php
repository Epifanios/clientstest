<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('/')->namespace('Admin')->group(function() {
    Route::match(['get', 'post'], '/', [App\Http\Controllers\AdminController::class, 'login']);

    Route::group(['middleware'=>['admin']],function () {
        Route::get('clientslist', [App\Http\Controllers\AdminController::class, 'clientslist']);
        Route::get('/logout', [App\Http\Controllers\AdminController::class, 'logout']);
        Route::match(['get', 'post'], '/add-edit-client/{id?}', [App\Http\Controllers\AdminController::class, 'AddEditClient']);
        Route::get('/delete-client/{id}', [App\Http\Controllers\AdminController::class, 'deleteClient']);
        Route::get('search-results-clients', [App\Http\Controllers\AdminController::class, 'search_results_clients']);

        //Route::get('employeeslist', [App\Http\Controllers\AdminController::class, 'employeeslist']);
        Route::match(['get', 'post'], '/employeeslist', [App\Http\Controllers\AdminController::class, 'employeeslist']);

    });
});
