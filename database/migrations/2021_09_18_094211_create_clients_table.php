<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('surname', 100);
            $table->string('email', 100);
            $table->bigInteger('work_phone')->default(8)->nullable();
            $table->bigInteger('mobile_phone')->default(8)->nullable();
            $table->string('address', 150);
            $table->string('post_code', 4);
            $table->string('country', 50);
            $table->string('birthday');
            $table->timestamps();
        });

        Schema::create('client_service', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
        Schema::dropIfExists('client_service');
    }
}
