<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employee;
use DB;
use File;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::truncate();
        $json = File::get('database/data/employees.json');
        $data = json_decode($json);

        foreach($data as $obj) {
            Employee::create(array(
                'employee_name' => $obj->employee_name,
                'employee_salary' => $obj->employee_salary,
                'employee_age' => $obj->employee_age,
                'profile_image' => $obj->profile_image,
            ));
        }
    }
}
