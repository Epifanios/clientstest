<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Client;
use App\Models\Service;
use Session;

class AdminController extends Controller
{
    public function login(Request $request) {
        // echo $password = Hash::make('12345678'); die;
        if($request->isMethod('post')) {
            $data = $request->all();

            $rules = [
                'email' => 'required|email|max:255',
                'password' => 'required',
            ];

            $customMessages = [
                'email.required' => 'Email Address is required!',
                'email.email' => 'Email Address is invalid!',
                'password.required' => 'Password is required!',
            ];

            $this->validate($request, $rules, $customMessages);

            if (Auth::guard('admin')->attempt(['email'=>$data['email'],'password'=>$data['password']])) {
                return redirect('/clientslist');
            } else {
                $request->session()->flash('error_message', 'Invalid Email Address or Password');
                return redirect()->back();
            }
        }
        return view('login');
    }

    public function clientslist() {
        $clients = Client::with('clients_services')->get();
        return view('clientslist')->with(compact('clients'));
    }

    public function logout() {
        Auth::guard('admin')->logout();
        return redirect('/');
    }

    public function AddEditClient(Request $request, $id=null) {
        if($id == "") {
            $title = "Add Client";
            $client = new Client;
            $clientData = array();
            $selectedServices = array();
            $message = "Client saved successfully!";
        } else {
            $title = "Edit Client";
            $clientData = Client::find($id);
            $clientData = json_decode(json_encode($clientData), true);
            $client = Client::findOrFail($id);
            $selectedServices = $client->clients_services->pluck('id')->toArray();
            $message = "Client updated successfully!";
        }

        if($request->isMethod('post')) {
            $data = $request->all();

            $rules = [
                'client_name' => 'required|max:100|regex:/^[\pL\s\-]+$/u',
                'client_surname' => 'max:100|regex:/^[\pL\s\-]+$/u|nullable',
                'client_email' => 'required|email',
                'work_phone' => 'required_without:mobile_phone|numeric|digits:8|nullable',
                'mobile_phone' => 'required_without:work_phone|numeric|digits:8|nullable',
                'client_address' => 'max:150',
                'client_post_code' => 'digits:4|nullable',
                'client_country' => 'max:50',
                'service_id' => 'required',
            ];

            $customMessages = [
                'client_name.required' => 'Client Name is required!',
                'client_name.regex' => 'Client Name is invalid!',
                'client_surname.regex' => 'Client Surname is invalid!',
                'client_email.required' => 'Email Address is required!',
                'client_email.email' => 'Email Address is invalid!',
                'service_id.required' => 'You must select service!',
            ];

            $this->validate($request, $rules, $customMessages);

            if(empty($data['client_surname'])) {
                $data['client_surname'] = "";
            }

            if(empty($data['client_address'])) {
                $data['client_address'] = "";
            }

            if(empty($data['client_post_code'])) {
                $data['client_post_code'] = "";
            }

            if(empty($data['client_country'])) {
                $data['client_country'] = "";
            }

            if(empty($data['client_birthday'])) {
                $data['client_birthday'] = "";
            }

            $client->name = $data['client_name'];
            $client->surname = $data['client_surname'];
            $client->email = $data['client_email'];
            $client->work_phone = $data['work_phone'];
            $client->mobile_phone = $data['mobile_phone'];
            $client->address = $data['client_address'];
            $client->post_code = $data['client_post_code'];
            $client->country = $data['client_country'];
            $client->birthday = $data['client_birthday'];
            $client->save();

            $client->clients_services()->sync($data['service_id'], false);

            $request->session()->flash('success_message', $message);
            return redirect('clientslist');
        }

        $services = Service::get();
        $services = json_decode(json_encode($services), true);

        return view('add_edit_client')->with(compact('title', 'services', 'clientData', 'client', 'selectedServices'));
    }

    public function deleteClient(Request $request, $id) {
        $client = Client::find($id);
        $client->clients_services()->detach();
        $client->delete();
        $message = 'Client has been deleted Successfully!';
        $request->session()->flash('success_message', $message);
        return redirect()->back();
    }

    public function search_results_clients(Request $request) {
        $search = $request->get('search_clients');
        $clients = Client::with('clients_services')->where('name', 'like', '%' .$search. '%')->orWhere('work_phone', 'like', '%' .$search. '%')->orWhere('mobile_phone', 'like', '%' .$search. '%')->get();
        return view('search_clientslist')->with(compact('clients'));
    }




    public function employeeslist() {
        //$clients = Client::with('clients_services')->get();
        return view('employeeslist');
    }

    public function AddEmployee() {
        return view('add_employee');
    }

}
