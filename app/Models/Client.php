<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    public function clients_services() {
        return $this->belongsToMany('App\Models\Service', 'client_service');
    }
}
