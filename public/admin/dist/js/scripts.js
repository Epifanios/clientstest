$(document).ready(function() {

    //Check Current Password for admins
    $("#currentPass").keyup(function() {
        var currentPass = $("#currentPass").val();

        $.ajax({
            type:'post',
            url:'/adminpanel/check-current-pass',
            data:{
                currentPass: currentPass,
            },
            success:function(resp) {
                if(resp == "false") {
                    $("#currentPass_error").html("<font color=red>Current Password is Incorrect</font>");
                } else if (resp == "true") {
                    $("#currentPass_error").html("<font color=green>Current Password is Correct</font>");
                }
            }, error:function() {

            }
        });
    });


    //Update Section Status
    $(".updateSectionStatus").click(function() {
        var status = $(this).children("i").attr("status");;
        var section_id = $(this).attr("section_id");
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/adminpanel/update-section-status',
            data: {
                status: status,
                section_id: section_id
            },
            success:function(resp) {
                if(resp['status'] == 0) {
                    $("#section-" + section_id).html("<i class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>");
                } else if(resp['status'] == 1){
                    $("#section-" + section_id).html("<i class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>");
                }
            },
            error:function() {
                alert("Error");
            }
        });
    });


    //Update Category Status
    $(".updateCategoryStatus").click(function() {
        var status = $(this).children("i").attr("status");;
        var category_id = $(this).attr("category_id");
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/adminpanel/update-category-status',
            data: {
                status: status,
                category_id: category_id
            },
            success:function(resp) {
                if(resp['status'] == 0) {
                    $("#category-" + category_id).html("<i class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>");
                } else if(resp['status'] == 1){
                    $("#category-" + category_id).html("<i class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>");
                }
            },
            error:function() {
                alert("Error");
            }
        });
    });


    //Append Category Level
    $("#section_id").change(function() {
        var section_id = $(this).val();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/adminpanel/append-categories-level',
            data: {
                section_id: section_id
            },
            success:function(resp) {
                $("#append-category-level").html(resp);
            },
            error:function() {
                alert("Error");
            }
        });
    });


    //Confirm Delete Record
    $(".confirmDelete").click(function() {
        var name = $(this).attr("name");

        if(confirm("Are you sure to delete this "+ name +"?")) {
            return true;
        }
        return false;
    });


    //Update Product Status
    $(".updateProductStatus").click(function() {
        var status = $(this).children("i").attr("status");;
        var product_id = $(this).attr("product_id");
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/adminpanel/update-product-status',
            data: {
                status: status,
                product_id: product_id
            },
            success:function(resp) {
                if(resp['status'] == 0) {
                    $("#product-" + product_id).html("<i class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>");
                } else if(resp['status'] == 1){
                    $("#product-" + product_id).html("<i class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>");
                }
            },
            error:function() {
                alert("Error");
            }
        });
    });


    // add row
    $("#addRow").click(function () {
        var html = '';
        html += '<div id="inputFormRow">';
        html += '<div class="input-group mb-3">';
        html += '<input type="text" name="title[]" class="form-control m-input mr-2" placeholder="Enter title" autocomplete="off" required="">';
        html += '<input type="text" name="sku[]" class="form-control m-input mr-2" placeholder="Enter Sku" autocomplete="off" required="">';
        html += '<input type="number" name="price[]" class="form-control m-input mr-2" placeholder="Enter Price" autocomplete="off" required="">';
        html += '<input type="number" name="stock[]" class="form-control m-input" placeholder="Enter Stock" autocomplete="off" required="">';
        html += '<div class="input-group-append">';
        html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
        html += '</div>';
        html += '</div>';

        $('#newRow').append(html);
    });

    // remove row
    $(document).on('click', '#removeRow', function () {
        $(this).closest('#inputFormRow').remove();
    });
});
